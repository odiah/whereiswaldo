package com.sysomos.location.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.com.bytecode.opencsv.CSVWriter;

import com.sysomos.location.exception.GeoLocationFinderException;

public class TextUtils {
	private static final Logger LOG = LoggerFactory.getLogger(TextUtils.class);

	public static <U, V> void dumpToCSV(File file, final Map<U, V> map)
			throws GeoLocationFinderException {
		dumpToCSV(file, map, null);
	}

	public static <U, V> void dumpToCSV(File file, final Map<U, V> map,
			final Map<U, V> exclude) throws GeoLocationFinderException {
		if (file == null || map == null) {
			throw new GeoLocationFinderException(
					"File cannot be found or locations are empty");
		}
		try {
			CSVWriter writer = new CSVWriter(new FileWriter(
					file.getAbsolutePath()));
			long time = new DateTime().getMillis();
			for (U u : map.keySet()) {
				if (exclude != null && exclude.containsKey(u))
					continue;
				String[] row = new String[] { u.toString(),
						map.get(u) == null ? "[null]" : map.get(u).toString(),
						String.valueOf(time) };
				writer.writeNext(row);
			}
			System.out.println("*******************************");
			LOG.info("Path to generated file: " + file.getAbsolutePath());
			writer.close();
		} catch (IOException e) {
			throw new GeoLocationFinderException(e);
		}
	}
}
