package com.sysomos.location.utils;


public interface Distance<T> {

	public double getDistance(T u, T v);
}
