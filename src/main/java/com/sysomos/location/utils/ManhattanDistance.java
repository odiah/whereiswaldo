package com.sysomos.location.utils;

import com.sysomos.location.exception.ManhattanDistanceException;

public class ManhattanDistance {

	public static double getDistance(double[] lhs, double[] rhs)
			throws ManhattanDistanceException {
		if (lhs == null || rhs == null || lhs.length != rhs.length)
			throw new ManhattanDistanceException("Invalid Arguments Exception");

		double distance = 0.0;
		for (int i = 0; i < lhs.length; i++) {
			distance += Math.abs(lhs[i] - rhs[i]);
		}
		return distance;
	}

}
