package com.sysomos.location.exception;

public class GeoLocationFinderException extends Exception {

	private static final long serialVersionUID = -6611428769319094677L;

	public GeoLocationFinderException(String message) {
		super(message);
	}

	public GeoLocationFinderException(Throwable cause) {
		super(cause);
	}

	public GeoLocationFinderException(Exception e) {
		super(e);
	}
}
