package com.sysomos.location.exception;

public class LPLocationPredictionException extends GeoLocationFinderException {

	private static final long serialVersionUID = -134244424840017885L;

	public LPLocationPredictionException(Throwable cause) {
		super(cause);
	}

	public LPLocationPredictionException(String message) {
		super(message);
	}

	public LPLocationPredictionException(Exception e) {
		super(e);
	}
}
