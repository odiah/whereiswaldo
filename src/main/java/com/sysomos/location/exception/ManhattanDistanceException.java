package com.sysomos.location.exception;

public class ManhattanDistanceException extends GeoLocationFinderException {

	private static final long serialVersionUID = -5767414327834546453L;

	public ManhattanDistanceException(Throwable cause) {
		super(cause);
	}

	public ManhattanDistanceException(String message) {
		super(message);
	}

	public ManhattanDistanceException(Exception e) {
		super(e);
	}
}
