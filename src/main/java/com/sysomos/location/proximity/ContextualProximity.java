package com.sysomos.location.proximity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Multimap;
import com.sysomos.activity.service.ActivityService;
import com.sysomos.activity.service.GetActivity;
import com.sysomos.activity.service.UserToUserActivity;
import com.sysomos.activity.service.exception.ActivityServiceException;
import com.sysomos.activity.service.phoenix.LookupUserToUserActivityQuery;
import com.sysomos.location.inference.data.FetchTweets;
import com.sysomos.location.utils.Pair;
import com.sysomos.location.utils.WordSequenceDistance;

public class ContextualProximity {

	private static final Logger LOG = LoggerFactory
			.getLogger(ContextualProximity.class);
	private static final Map<Long, Integer> originalTweets = new HashMap<Long, Integer>();

	public static Map<Pair<Long, Long>, Double> compute(
			final Multimap<Long, Long> followers, long startDateMillis,
			long endDateMillis, float gamma) {

		System.out.println("*******************************");
		System.out.println(
				"ContextualProximity.compute: start " + new DateTime());

		if (followers == null)
			return null;
		List<Future<Map<Pair<Long, Long>, Double>>> tasks = initTasks(followers,
				startDateMillis, endDateMillis);

		try {
			System.out.println("Calling the tasks");
			Map<Pair<Long, Long>, Double> activities = tasks.get(0).get();
			Map<Pair<Long, Long>, Double> contentSim = tasks.get(1).get();

			System.out.println(
					"ContextualProximity.compute: end " + new DateTime());
			return getProximity(contentSim, activities, gamma);
		} catch (InterruptedException e) {
			LOG.error(e.getLocalizedMessage(), e);
			return null;
		} catch (ExecutionException e) {
			LOG.error(e.getLocalizedMessage(), e);
			return null;
		}
	}

	private static List<Future<Map<Pair<Long, Long>, Double>>> initTasks(
			final Multimap<Long, Long> followers, final long startDateMillis,
			final long endDateMillis) {
		ExecutorService service = Executors.newSingleThreadExecutor();
		List<Future<Map<Pair<Long, Long>, Double>>> tasks;
		tasks = new ArrayList<Future<Map<Pair<Long, Long>, Double>>>();

		Callable<Map<Pair<Long, Long>, Double>> callable;
		
		callable = new Callable<Map<Pair<Long, Long>, Double>>() {
			public Map<Pair<Long, Long>, Double> call() throws Exception {
				return getUserToUserActivities(followers, startDateMillis,
						endDateMillis);
			}
		};
		tasks.add(service.submit(callable));
		callable = new Callable<Map<Pair<Long, Long>, Double>>() {
			public Map<Pair<Long, Long>, Double> call() throws Exception {
				return getTextualProximity(followers, startDateMillis,
						endDateMillis);
			}
		};
		tasks.add(service.submit(callable));
		service.shutdown();
		return tasks;
	}

	private static Map<Pair<Long, Long>, Double> getTextualProximity(
			Multimap<Long, Long> followers, long startDateMillis,
			long endDateMillis) {
		if (followers == null || endDateMillis < startDateMillis)
			return null;

		Set<Long> ids = new HashSet<Long>(followers.keySet());
		ids.addAll(followers.values());
		List<Long> userIds = new ArrayList<Long>(ids);

		Map<Long, Map<String, Double>> userIdWeights = FetchTweets.fetch(
				ImmutableList.copyOf(userIds), startDateMillis, endDateMillis,
				originalTweets);

		if (userIdWeights == null)
			return null;
		Map<Pair<Long, Long>, Double> weights = new HashMap<Pair<Long, Long>, Double>();
		for (long id1 : followers.keySet()) {
			for (long id2 : followers.get(id1)) {
				Pair<Long, Long> pair = new Pair<Long, Long>(id1, id2);

				Map<String, Double> aMap = userIdWeights.get(pair.getA());
				Map<String, Double> bMap = userIdWeights.get(pair.getB());

				double weight = WordSequenceDistance.getDistance(aMap, bMap);
				weight = Math.exp((-1 * Math.pow(weight, 2)) / 2);
				weights.put(pair, weight);

				pair = new Pair<Long, Long>(id2, id1);
				weights.put(pair, weight);
			}
		}
		return weights;
	}

	private static Map<Pair<Long, Long>, Double> getUserToUserActivities(
			final Multimap<Long, Long> followers, final long startDateMillis,
			final long endDateMillis) {
		// ExecutorService service = Executors.newSingleThreadExecutor();
		// List<Future<Map<Pair<Long, Long>, Integer>>> futures;
		// futures = new ArrayList<Future<Map<Pair<Long, Long>, Integer>>>();
		//
		// for (long id : followers.keySet()) {
		// final List<Long> userIds = new ArrayList<Long>(followers.get(id));
		// userIds.add(id);
		// Callable<Map<Pair<Long, Long>, Integer>> callable;
		// callable = new Callable<Map<Pair<Long, Long>, Integer>>() {
		//
		// @Override
		// public Map<Pair<Long, Long>, Integer> call() throws Exception {
		// return getUserToUserActivities(userIds, startDateMillis,
		// endDateMillis);
		// }
		// };
		// futures.add(service.submit(callable));
		// }
		// service.shutdown();
		// Map<Pair<Long, Long>, Integer> activities = new HashMap<Pair<Long,
		// Long>, Integer>();
		// for (int i = 0; i < futures.size(); i++) {
		// try {
		// Future<Map<Pair<Long, Long>, Integer>> task = futures.get(i);
		// if (MapUtils.isEmpty(task.get()))
		// continue;
		// activities.putAll(task.get());
		// } catch (InterruptedException e) {
		// LOG.error(e.getLocalizedMessage(), e);
		// } catch (ExecutionException e) {
		// LOG.error(e.getLocalizedMessage(), e);
		// }
		// }

		Map<Pair<Long, Long>, Double> activities = new HashMap<Pair<Long, Long>, Double>();
		List<Long> keys = new ArrayList<Long>(followers.keySet());
		List<Long> vals = new ArrayList<Long>(followers.values());
		activities.putAll(getUserToUserActivities(keys, vals, startDateMillis,
				endDateMillis));

		System.out.println("*** UserToUserActivity Size: "
				+ (activities == null ? 0 : activities.size()));
		return activities;
	}

	private static Map<Pair<Long, Long>, Double> getUserToUserActivities(
			List<Long> userIds, List<Long> refUserIds, long startDateMillis,
			long endDateMillis) {

		System.out.println(
				"*** Calling ActivityService: start " + new DateTime());

		GetActivity<List<UserToUserActivity>, LookupUserToUserActivityQuery>.Batch batch = ActivityService
				.newGetUserToUserActivityBatch();
		batch.addRequest(userIds, refUserIds, startDateMillis, endDateMillis);
		GetActivity<List<UserToUserActivity>, LookupUserToUserActivityQuery>.ResultContext results = batch
				.call();
		if (results == null || results.size() == 0) {
			String msg = "Results are empty. No activity found between seed "
					+ "user and his/her followers/friends";
			LOG.warn(msg);
			return null;
		}

		Map<Pair<Long, Long>, Double> activities = new HashMap<Pair<Long, Long>, Double>();
		for (int i = 0; i < results.size(); i++) {
			try {
				List<UserToUserActivity> activs = results.get(i);
				if (CollectionUtils.isEmpty(activs))
					continue;
				for (UserToUserActivity u2u : activs) {
					if (u2u == null || u2u.getEngagement() == 0)
						continue;
					Pair<Long, Long> pair = new Pair<Long, Long>(
							u2u.getIdPair().getA(), u2u.getIdPair().getB());
					activities.put(pair, (double) u2u.getEngagement());
				}
			} catch (ActivityServiceException e) {
				LOG.error(e.getLocalizedMessage(), e);
			}
		}

		System.out.println(
				"*** Call to ActivityService: end   " + new DateTime());

		return activities;
	}

	private static Map<Pair<Long, Long>, Double> getProximity(
			Map<Pair<Long, Long>, Double> activities,
			Map<Pair<Long, Long>, Double> userIdWeights, float gamma) {
		Map<Pair<Long, Long>, Double> weights = new HashMap<Pair<Long, Long>, Double>();
		for (Map.Entry<Pair<Long, Long>, Double> e : activities.entrySet()) {
			Pair<Long, Long> pair = e.getKey();
			weights.put(pair,
					getProximity(pair, userIdWeights, activities, gamma));
		}
		return weights;
	}

	private static Double getProximity(Pair<Long, Long> pair,
			Map<Pair<Long, Long>, Double> activities,
			Map<Pair<Long, Long>, Double> userIdWeights, float gamma) {

		double weight = userIdWeights.containsKey(pair)
				? userIdWeights.get(pair) : 0.0;

		double engagement = getEngagement(pair, activities, originalTweets);
		double proximity = gamma * weight + (1 - gamma) * engagement;

		return proximity;
	}

	private static double getEngagement(Pair<Long, Long> pair,
			Map<Pair<Long, Long>, Double> activities,
			Map<Long, Integer> originalTweets) {

		if (MapUtils.isEmpty(activities))
			return 0.0;

		double activity = activities.containsKey(pair) ? activities.get(pair)
				: 0;

		Integer numOriginalTweet = originalTweets.get(pair.getB());
		if (numOriginalTweet == null || numOriginalTweet == 0.0) {
			return 0.0;
		}

		double engagement = numOriginalTweet >= activity
				? (1.0 * activity) / numOriginalTweet
				: (1.0 * activity / (activity + numOriginalTweet));

		return engagement;
	}

}
