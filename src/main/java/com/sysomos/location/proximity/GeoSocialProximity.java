package com.sysomos.location.proximity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.joda.time.DateTime;
import org.springframework.util.CollectionUtils;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;
import com.sysomos.location.core.UserLocation;
import com.sysomos.location.exception.FetchFriendsException;
import com.sysomos.location.exception.GeoSocialProximityException;
import com.sysomos.location.inference.data.FetchFriends;
import com.sysomos.location.utils.Pair;

public class GeoSocialProximity {

	private static final int MAX_FRIEND_LIMIT = 2000;

	public static Map<Pair<Long, Long>, Double> compute(
			final Map<Long, UserLocation> seeds,
			final ImmutableMultimap<Long, Long> followers)
					throws GeoSocialProximityException {

		System.out.println("*******************************");
		System.out
				.println("GeoSocialProximity.compute: start " + new DateTime());

		Multimap<Long, Long> friends = null;
		try {
			friends = FetchFriends.fetch(ImmutableList.copyOf(seeds.keySet()),
					MAX_FRIEND_LIMIT);
		} catch (FetchFriendsException e) {
			throw new GeoSocialProximityException(e);
		}

		if (CollectionUtils.isEmpty(seeds) || followers == null
				|| friends == null)
			return null;

		friends.putAll(followers.inverse());
		Map<Pair<Long, Long>, Double> proximityMap;
		proximityMap = new HashMap<Pair<Long, Long>, Double>();

		for (long id2 : seeds.keySet()) {
			for (long id1 : friends.get(id2)) {

				if (proximityMap.containsKey(new Pair<Long, Long>(id1, id2)))
					continue;
				double proximity = (id1 == id2) ? 1.0
						: compute(id1, id2, friends, seeds);

				Pair<Long, Long> pair = new Pair<Long, Long>(id1, id2);
				proximityMap.put(pair, proximity);
				if (id1 == id2)
					continue;
				pair = new Pair<Long, Long>(id2, id1);
				proximityMap.put(pair, proximity);
			}
		}
		System.out.println("GeoSocialProximity.compute: end " + new DateTime());

		return proximityMap;
	}

	private static double compute(long userId1, long userId2,
			final Multimap<Long, Long> friends,
			final Map<Long, UserLocation> seeds)
					throws GeoSocialProximityException {
		return compute(userId1, userId2, friends, seeds, 0);
	}

	private static double compute(long userId1, long userId2,
			final Multimap<Long, Long> friends,
			final Map<Long, UserLocation> seeds, double proximity)
					throws GeoSocialProximityException {

		if (friends == null)
			throw new GeoSocialProximityException("Friends map is null");

		List<Long> friendsU1 = new ArrayList<Long>(friends.get(userId1));
		List<Long> friendsU2 = new ArrayList<Long>(friends.get(userId2));

		Map<Long, UserLocation> common = filter(friendsU1, friendsU2, seeds);

		if (MapUtils.isEmpty(common))
			return 0.0;

		final UserLocation ul1 = seeds.get(userId1);
		final UserLocation ul2 = seeds.get(userId2);

		if (ul1 == null && ul2 == null)
			throw new GeoSocialProximityException("Users " + userId1 + " and "
					+ userId2 + " locations aren't specified.");

		if (ul1 != null && ul2 != null) {
			proximity = Math.max(proximity, getProximity(ul1, ul2, common));
			return proximity;
		}

		if (ul1 == null) {
			for (long id : common.keySet()) {
				proximity = Math.max(proximity,
						compute(userId2, id, friends, seeds, proximity));
			}
		} else {
			for (long id : common.keySet()) {
				proximity = Math.max(proximity,
						compute(userId1, id, friends, seeds, proximity));
			}
		}
		return proximity;
	}

	private static Map<Long, UserLocation> filter(final Collection<Long> list1,
			final Collection<Long> list2, final Map<Long, UserLocation> seeds) {

		if (CollectionUtils.isEmpty(list1) || CollectionUtils.isEmpty(list2))
			return null;
		list1.retainAll(list2);
		Map<Long, UserLocation> filtered = new HashMap<Long, UserLocation>();
		for (long userId : list1) {
			if (seeds.containsKey(userId)) {
				filtered.put(userId, seeds.get(userId));
			}
		}
		return filtered;
	}

	private static double getProximity(final UserLocation ul1,
			final UserLocation ul2, final Map<Long, UserLocation> common) {
		if (MapUtils.isEmpty(common))
			return 0.0;
		int cardinality = common.size(), count = 0;
		double radius = getDistance(ul1, ul2);
		for (Map.Entry<Long, UserLocation> entry : common.entrySet()) {
			double distance = getDistance(ul1, entry.getValue());
			if (distance <= radius) {
				count++;
			}
		}
		double proximity = (1.0 * count) / cardinality;
		return proximity;
	}

	private static double getDistance(final UserLocation ul1,
			final UserLocation ul2) {
		if (ul1 == null || ul2 == null)
			return 0.0;
		double distance = Math.sqrt(Math.pow((ul1.latitude - ul2.latitude), 2)
				+ Math.pow((ul1.longitude - ul2.longitude), 2));
		return distance;
	}
}
