package com.sysomos.location.core;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.IStringConverter;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;
import com.sysomos.location.exception.GeoLocationFinderException;
import com.sysomos.location.inference.NearestNeighbor;
import com.sysomos.location.inference.args.KNNLocationInferenceArgs;
import com.sysomos.location.inference.args.LPLocationInferenceArgs;
import com.sysomos.location.inference.args.LocationInferenceArgs;
import com.sysomos.location.inference.data.FetchFollowers;
import com.sysomos.location.proximity.ContextualProximity;
import com.sysomos.location.proximity.GeoSocialProximity;
//import com.sysomos.location.utils.ManhattanDistance;
import com.sysomos.location.utils.Pair;
import com.sysomos.location.utils.SamplingUtils;
import com.sysomos.location.utils.TextUtils;

import au.com.bytecode.opencsv.CSVReader;

public class GeoLocationFinder {

	private static final Logger LOG = LoggerFactory
			.getLogger(GeoLocationFinder.class);
	private static final long StartDateMillis, EndDateMillis;

	static {
		StartDateMillis = new DateTime().minusDays(3).getMillis();
		EndDateMillis = new DateTime().minusDays(1).getMillis();
	}

	private static final class SeedListFetcher {
		private Map<Long, UserLocation> seeds;

		public SeedListFetcher(String filename) throws IOException {

			seeds = new HashMap<Long, UserLocation>();
			File file = new File(filename);
			FileReader reader = new FileReader(file);
			CSVReader csvReader = new CSVReader(reader, ';');

			String[] values = csvReader.readNext();

			while (values != null) {
				if (values.length < 4)
					continue;
				long userId = Long.parseLong(values[0]);
				seeds.put(userId, new UserLocation(Float.valueOf(values[2]),
						Float.valueOf(values[3]), values[1]));
				values = csvReader.readNext();
			}
			csvReader.close();
		}
	}

	public static class SeedListFetcherConverter
			implements IStringConverter<SeedListFetcher> {
		@Override
		public SeedListFetcher convert(String filename) {
			if (StringUtils.isEmpty(filename))
				return null;
			try {
				return new SeedListFetcher(filename);
			} catch (IOException e) {
				LOG.error(e.getLocalizedMessage(), e);
			}
			return null;
		}
	}

	public static class HyperParameter implements IParameterValidator {
		@Override
		public void validate(String name, String value)
				throws ParameterException {
			float weight = Float.parseFloat(value);
			if (weight < 0 || weight > 1) {
				throw new ParameterException("Parameter " + name
						+ " should be in [0, 1] (found " + value + ")");
			}
		}
	}

	public static class PositiveInteger implements IParameterValidator {
		@Override
		public void validate(String name, String value)
				throws ParameterException {
			int n = Integer.parseInt(value);
			if (n <= 0) {
				throw new ParameterException("Parameter " + name
						+ " should be positive (found " + value + ")");
			}
		}
	}

	public static class FileConverter implements IStringConverter<File> {
		@Override
		public File convert(String filename) {
			try {
				if (StringUtils.isEmpty(filename)) {
					filename = "LocationInference";
				}

				int index = filename.indexOf(".");
				String name = index >= 0 ? filename.substring(0, index)
						: filename;
				String type = index >= 0 ? filename.substring(index) : ".csv";
				filename = new StringBuilder().append(name).append("_")
						.append(new SimpleDateFormat("yyyyMMddhhmmss")
								.format(new Date()))
						.append(type).toString();

				if (!filename.contains(File.separator)) {
					String dir = System.getProperty("user.dir");
					filename = dir + File.separator + filename;
				}
				new FileOutputStream(filename, false).close();
				return new File(filename);
			} catch (IOException e) {
				LOG.error(e.getLocalizedMessage(), e);
			}
			return null;
		}
	}

	private static enum LocationInferenceMethod {
		NEAREST_NEIGHBORS(1) {
			@Override
			public Map<Long, UserLocation> infer(
					final LocationInferenceArgs args)
							throws GeoLocationFinderException {

				if (args == null)
					return null;
				KNNLocationInferenceArgs kNNargs = (KNNLocationInferenceArgs) args;
				// Map<Pair<Long, Long>, Double> distance = new
				// HashMap<Pair<Long, Long>, Double>();
				// for (Map.Entry<Pair<Long, Long>, Double> entry :
				// kNNargs.proximity
				// .entrySet()) {
				// Pair<Long, Long> pair = new Pair<Long, Long>(
				// entry.getKey().getB(), entry.getKey().getA());
				// if (distance.containsKey(pair)) {
				// distance.put(entry.getKey(), distance.get(pair));
				// continue;
				// }
				// distance.put(entry.getKey(),
				// ManhattanDistance.getDistance(new double[] {
				// entry.getValue() },
				// new double[] { kNNargs.proximity.get(pair) }));
				// }
				return NearestNeighbor.knn(kNNargs.proximity, kNNargs.seeds,
						kNNargs.numNearestNeighbors);
			}
		},
		LABEL_PROPAGATION(2) {
			@Override
			public Map<Long, UserLocation> infer(
					final LocationInferenceArgs args) {
				LPLocationInferenceArgs lpArgs = (LPLocationInferenceArgs) args;
				return null;
			}
		};

		public int method;

		private LocationInferenceMethod(int method) {
			this.method = method;
		}

		public abstract Map<Long, UserLocation> infer(
				final LocationInferenceArgs args)
						throws GeoLocationFinderException;

		public static LocationInferenceMethod get(int method) {
			for (LocationInferenceMethod predictor : values()) {
				if (predictor.method == method)
					return predictor;
			}
			return null;
		}
	}

	public static class Parameters {
		@Parameter(names = { "-infile",
				"-i" }, required = true, description = "File containing seeds to infer "
						+ "geographic locations of other users from", converter = SeedListFetcherConverter.class)
		private SeedListFetcher seedListFetcher = null;
		@Parameter(names = { "-alpha",
				"-a" }, required = true, description = "Hyper-parameter used when computing "
						+ "contextual proximity. Should be between 0 and 1", validateWith = HyperParameter.class)
		private float alpha;
		@Parameter(names = { "-gamma",
				"-g" }, required = true, description = "Hyper-parameter used when computing "
						+ "social-spatial proximity. Should be between 0 and 1", validateWith = HyperParameter.class)
		private float gamma;
		@Parameter(names = "-k", required = false, description = "Number of nearest neighbors to consider "
				+ "when k-nn is used", validateWith = PositiveInteger.class)
		private int k = 100;
		@Parameter(names = { "-outfile",
				"-o" }, required = false, description = "File containing geo locations "
						+ "of users whose geographic locations are inferred", converter = FileConverter.class)
		private File dumpFile = null;
		@Parameter(names = { "-feeds",
				"-f" }, required = false, description = "Twitter Feeds file from SOLR Batch API")
		private String feeds = "TwitterFeeds.gz";
		@Parameter(names = { "-method",
				"-m" }, required = true, description = "Statistical/Mathematical method "
						+ "to use to infer/predict geographical locations of users")
		public int method;
	}

	public void find(final Parameters params)
			throws GeoLocationFinderException {
		if (params.seedListFetcher == null) {
			throw new GeoLocationFinderException("File not found.");
		}
		ImmutableMultimap<Long, Long> followers = FetchFollowers.fetch(
				ImmutableList.copyOf(params.seedListFetcher.seeds.keySet()));
		ImmutableMultimap<Long, Long> samples = sample(followers);
		Map<Pair<Long, Long>, Double> proximity = compute(params, samples);
		LocationInferenceMethod method = LocationInferenceMethod
				.get(params.method);

		LocationInferenceArgs args = null;

		switch (method) {
		case NEAREST_NEIGHBORS:
			args = new KNNLocationInferenceArgs(params.seedListFetcher.seeds,
					proximity, params.k);
			break;
		case LABEL_PROPAGATION:
			throw new GeoLocationFinderException(
					"Unsupported Location Inference Algorithm");
		default:
			args = new KNNLocationInferenceArgs(params.seedListFetcher.seeds,
					proximity, params.k);
		}

		System.out.println(
				"*** LocationInferenceMethod.infer: start " + new DateTime());

		Map<Long, UserLocation> inferred = method.infer(args);

		System.out.println(
				"*** LocationInferenceMethod.infer: end " + new DateTime());

		inferred = GeoLocationWrapper.decorate(inferred);
		TextUtils.dumpToCSV(params.dumpFile, inferred,
				params.seedListFetcher.seeds);
	}

	private Map<Pair<Long, Long>, Double> compute(final Parameters params,
			final ImmutableMultimap<Long, Long> followers)
					throws GeoLocationFinderException {

		ExecutorService service = Executors.newSingleThreadExecutor();
		List<Future<Map<Pair<Long, Long>, Double>>> futures;
		futures = new ArrayList<Future<Map<Pair<Long, Long>, Double>>>();

		Callable<Map<Pair<Long, Long>, Double>> callable;
		callable = new Callable<Map<Pair<Long, Long>, Double>>() {
			public Map<Pair<Long, Long>, Double> call() throws Exception {
				return ContextualProximity.compute(followers, StartDateMillis,
						EndDateMillis, params.gamma);
			}
		};
		futures.add(service.submit(callable));

		callable = new Callable<Map<Pair<Long, Long>, Double>>() {
			public Map<Pair<Long, Long>, Double> call() throws Exception {
				return GeoSocialProximity.compute(params.seedListFetcher.seeds,
						followers);
			}
		};
		futures.add(service.submit(callable));
		service.shutdown();

		try {
			Map<Pair<Long, Long>, Double> conProximity = futures.get(0).get();
			Map<Pair<Long, Long>, Double> socProximity = futures.get(1).get();
			Map<Pair<Long, Long>, Double> proximity = compute(conProximity,
					socProximity, params.alpha);
			return proximity;
		} catch (ExecutionException e) {
			throw new GeoLocationFinderException(e);
		} catch (InterruptedException e) {
			throw new GeoLocationFinderException(e);
		}
	}

	private ImmutableMultimap<Long, Long> sample(
			ImmutableMultimap<Long, Long> followers) {
		Multimap<Long, Long> toReturn = ArrayListMultimap.create();
		for (long id : followers.keySet()) {
			if (followers.get(id).size() > 10000) {
				List<Long> userids = new ArrayList<Long>(followers.get(id));
				toReturn.putAll(id, SamplingUtils.reservoir(userids, 10000));
			} else {
				toReturn.putAll(id, followers.get(id));
			}
		}
		return ImmutableMultimap.copyOf(toReturn);
	}

	private Map<Pair<Long, Long>, Double> compute(
			final Map<Pair<Long, Long>, Double> conProximity,
			final Map<Pair<Long, Long>, Double> socProximity, float alpha)
					throws GeoLocationFinderException {
		if (conProximity == null || socProximity == null || alpha < 0
				|| alpha > 1) {
			String message = "Exception. Check one of the following: 1. conPromixity is null, "
					+ "2. socProximity is null, 3. alpha is less than 0 or greater than 1.";
			throw new GeoLocationFinderException(message);
		}

		System.out.println("*******************************");
		System.out
				.println("GeoLocationFinder.compute: start " + new DateTime());

		Set<Pair<Long, Long>> allKeys = new HashSet<Pair<Long, Long>>(
				conProximity.keySet());
		allKeys.addAll(socProximity.keySet());

		Map<Pair<Long, Long>, Double> weights = new HashMap<Pair<Long, Long>, Double>();

		for (Pair<Long, Long> pair : allKeys) {
			Double value = conProximity.get(pair);
			value = value == null ? 0.0 : value;
			double proximity = alpha * value;

			value = socProximity.get(pair);
			value = value == null ? 0.0 : value;
			proximity += (1 - alpha) * value;

			weights.put(pair, proximity);
		}
		System.out.println("*** Proximity Weights Map Size: " + weights.size());
		System.out.println("GeoLocationFinder.compute: end " + new DateTime());
		return weights;
	}

	public static void main(String args[]) {
		Parameters params = new Parameters();
		new JCommander(params, args).parse();
		GeoLocationFinder finder = new GeoLocationFinder();
		try {
			finder.find(params);
		} catch (GeoLocationFinderException e) {
			LOG.error(e.getLocalizedMessage(), e);
		}
	}
}
