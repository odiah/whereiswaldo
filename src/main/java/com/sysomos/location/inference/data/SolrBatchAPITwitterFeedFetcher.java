package com.sysomos.location.inference.data;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.io.FileUtils;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.sysomos.activity.service.utils.ListUtils;
import com.sysomos.location.inference.data.ResponseEntity.JobDetails;

public class SolrBatchAPITwitterFeedFetcher implements FeedFetcher<File> {

	private static final String SOLR_URL = "http://10.10.0.141:80/api/rest/v1.3/batch/post";
	private static final String MEDIA_MGR = "http://10.10.0.141:80/api/rest/v1.3/mediamgr/post/%s";
	// appId, userId, requestId
	private static final String POST_STATUS = "http://10.10.0.141:80/api/rest/v1.3/batch/post/%s/%s/%s/status";
	private static final Logger LOG = LoggerFactory
			.getLogger(SolrBatchAPITwitterFeedFetcher.class);

	private static final String APP_ID = "geo";
	private static final String APP_ID_ID = "12345";
	private static final String TWITTER_FEEDS = "/tmp/TwitterFeeds";

	private static final int BATCH_SIZE = 5000;
	private static int requestId = new Random().nextInt(1500000);

	private final AtomicInteger countInBatch;
	private ConcurrentLinkedQueue<HttpUriRequest> queue;

	private HttpClient httpclient;

	public SolrBatchAPITwitterFeedFetcher() {
		this.countInBatch = new AtomicInteger(0);
		queue = new ConcurrentLinkedQueue<HttpUriRequest>();
		httpclient = HttpClientBuilder.create().build();
	}

	public static enum RequestType {
		GET, POST
	}

	public static class Builder {

		private RequestType type;
		private Collection<Long> userIds;
		private long startDateMillis;
		private long endDateMillis;

		public Builder(RequestType type, Collection<Long> userIds,
				long startDateMillis, long endDateMillis) {
			if (CollectionUtils.isEmpty(userIds))
				throw new RuntimeException(
						"List of User IDs shouldn't be null");
			this.type = type;
			this.userIds = userIds;
			this.startDateMillis = startDateMillis;
			this.endDateMillis = endDateMillis;
		}

		public SolrBatchAPITwitterFeedFetcher build() {
			List<String> queries = new ArrayList<String>();
			List<String> ttUIDs = new ArrayList<String>();
			for (Long id : userIds) {
				if (ttUIDs.size() < BATCH_SIZE) {
					ttUIDs.add("actorId:TT" + id);
				} else {
					queries.add(ListUtils.join(ttUIDs, " OR "));
					ttUIDs = new ArrayList<String>();
				}
			}
			if (!CollectionUtils.isEmpty(ttUIDs)) {
				queries.add(ListUtils.join(ttUIDs, " OR "));
			}
			SolrBatchAPITwitterFeedFetcher fetcher;
			fetcher = new SolrBatchAPITwitterFeedFetcher();

			if (type == RequestType.GET) {
				for (String query : queries) {
					fetcher.queue.add(fetcher.createGetJob(query,
							startDateMillis, endDateMillis));
				}
			} else {
				for (String query : queries) {
					Map<String, String> params = new HashMap<String, String>();
					params.put("appId", APP_ID);
					params.put("userId", APP_ID_ID);
					params.put("query", query);
					params.put("startDate", String.valueOf(startDateMillis));
					params.put("endDate", String.valueOf(endDateMillis));
					params.put("dataSrc", "TT");
					params.put("sortBy", "createDate");

					requestId += new Random().nextInt(500000);
					params.put("requestId", String.valueOf(requestId));

					fetcher.queue.add(fetcher.createPostJob(params));
				}
			}
			return fetcher;
		}
	}

	private HttpUriRequest createPostJob(final Map<String, String> params) {
		if (params == null || params.size() == 0)
			return null;
		List<NameValuePair> pairs = new ArrayList<NameValuePair>(params.size());
		for (Map.Entry<String, String> e : params.entrySet()) {
			pairs.add(new BasicNameValuePair(e.getKey(), e.getValue()));
		}

		UrlEncodedFormEntity encodedEntity = null;
			try {
				encodedEntity = new UrlEncodedFormEntity(pairs,
						"UTF-8");
			} catch (UnsupportedEncodingException e1) {
				LOG.error("SET WRONG ENCODING: ", e1);
			}
		HttpPost httppost = new HttpPost(SOLR_URL + "/query");
		httppost.setEntity(encodedEntity);
		System.out.println("enc " + encodedEntity.toString());
		return httppost;
	}

	private HttpUriRequest createGetJob(String ids, long startDateMillis,
			long endDateMillis) {
		try {
			StringBuilder qb = new StringBuilder();
			qb.append(SOLR_URL + "?");
			qb.append("appId=");
			qb.append(APP_ID);
			qb.append("&");
			qb.append("userId=");
			qb.append(APP_ID_ID);
			qb.append("&");
			qb.append("query=");
			qb.append(UTF8Encoder.encode(ids));
			qb.append("&");
			qb.append("startDate=");
			qb.append(startDateMillis);
			qb.append("&");
			qb.append("endDate=");
			qb.append(endDateMillis);
			qb.append("&");
			qb.append("dataSrc=TT");
			qb.append("&");
			qb.append("sortBy=");
			qb.append("createDate");
			qb.append("&");

			requestId += new Random().nextInt(500000);
			qb.append("requestId=").append(requestId);
			System.out.println(qb.toString());

			return new HttpGet(new URI(qb.toString()).toString());

		} catch (URISyntaxException e) {
			LOG.error("Query: [ " + ids + " ] , Exception: {}",
					e.getLocalizedMessage(), e);
		}
		return null;
	}

	private void fetch(List<JobDetails> jobDetails) throws Throwable {
		int index = 0, failed = 0;
		if (CollectionUtils.isEmpty(jobDetails))
			return;
		for (JobDetails job : jobDetails) {
			int backoff = 10 * 1 * 1000;
			while (true) {
				String checkStatusUrl = String.format(POST_STATUS, job.appId,
						job.userId, job.requestId);
				HttpEntity resEntityGet = httpEntity(checkStatusUrl);
				ResponseEntity entity = serialize(resEntityGet,
						ResponseEntity.class);
				List<JobDetails> jobs = entity == null ? null : entity.results;
				if (CollectionUtils.isEmpty(jobs))
					break;

				JobDetails detail = jobs.get(0);
				if (detail == null)
					break;
				if ("COMPLETED".equalsIgnoreCase(detail.status)) {
					String filename = TWITTER_FEEDS + "/TwitterFeeds"
							+ (index++) + ".gz";
					System.out.println("Generating File " + filename);
					File file = new File(filename);
					FileUtils.copyURLToFile(
							new URL(String.format(MEDIA_MGR, job.jobId)), file);
					break;
				}
				if ("FAILED".equalsIgnoreCase(detail.status)) {
					System.out
							.println("Job [ " + detail + " ] failed. Exiting.");
					break;
				}
				try {
					Thread.sleep(backoff);
					backoff *= 2;
					System.out.println("Sleeping for " + backoff
							+ " milliseconds before retry. Job [ " + detail
							+ " ].");
				} catch (InterruptedException e) {
					LOG.error(e.getLocalizedMessage(), e);
					failed++;
				}
			}
		}
		if (failed == jobDetails.size()) {
			throw new RuntimeException("Cannot fetch feeds. All jobs failed");
		}
	}

	@Override
	public File fetch() throws Throwable {
		int batchSize = 20, size, old;
		File dir = new File(TWITTER_FEEDS);
		if (dir.exists()) {
			String[] entries = dir.list();
			for (String s : entries) {
				File file = new File(dir.getPath(), s);
				file.delete();
			}
		} else if (!dir.mkdir()) {
			throw new RuntimeException("Couldn't create folder " + TWITTER_FEEDS
					+ " to store the feeds");
		}
		List<JobDetails> jobDetails = new ArrayList<JobDetails>();
		while (!queue.isEmpty()) {
			if (queue.size() < batchSize) {
				List<HttpUriRequest> requests = newBatch(queue.size());
				saveJobDetails(requests, jobDetails);
				break;
			}
			do {
				old = countInBatch.get();
				size = (old + 1) % batchSize;
			} while (!countInBatch.compareAndSet(old, size));
			if (size == 0) {
				List<HttpUriRequest> requests = newBatch(batchSize);
				saveJobDetails(requests, jobDetails);
				if (!queue.isEmpty()) {
					try {
						Thread.sleep(10000);
					} catch (InterruptedException e) {
						LOG.error(e.getLocalizedMessage(), e);
					}
				}
			}
		}
		fetch(jobDetails);
		return dir;
	}

	private HttpEntity httpEntity(String query)
			throws URISyntaxException, ClientProtocolException, IOException {
		HttpGet get = new HttpGet(new URI(query).toString());
		HttpResponse responseGet = httpclient.execute(get);
		HttpEntity resEntityGet = responseGet.getEntity();
		return resEntityGet;
	}

	private List<HttpUriRequest> newBatch(int size) {
		List<HttpUriRequest> batch = Lists.newArrayListWithCapacity(size);
		for (int i = 0; i < size; i++) {
			HttpUriRequest entry = queue.remove();
			if (entry == null) {
				LOG.warn("Queue has fewer elements than expected: "
						+ batch.size() + " instead of " + size);
				break;
			}
			batch.add(entry);
		}
		return batch;
	}

	private void saveJobDetails(List<HttpUriRequest> requests,
			List<JobDetails> jobDetails) {
		List<ResponseEntity> responses = serialize(execute(requests),
				ResponseEntity.class);
		if (CollectionUtils.isEmpty(responses))
			return;
		for (ResponseEntity resp : responses) {
			if (resp == null)
				continue;
			List<JobDetails> jobs = resp == null ? null : resp.results;
			if (CollectionUtils.isEmpty(jobs))
				continue;
			jobDetails.add(jobs.get(0));
		}
	}

	private List<HttpEntity> execute(List<HttpUriRequest> requests) {
		if (CollectionUtils.isEmpty(requests))
			return null;
		List<HttpEntity> entities = new ArrayList<HttpEntity>();
		for (HttpUriRequest entry : requests) {
			try {
				HttpResponse responseGet = httpclient.execute(entry);
				System.out.println(responseGet.getStatusLine());
				entities.add(responseGet.getEntity());
			} catch (IOException e) {
				LOG.error(e.getLocalizedMessage(), e);
			}
		}
		return entities;
	}

	private <T> List<T> serialize(List<HttpEntity> resEntities,
			Class<T> clazz) {
		if (CollectionUtils.isEmpty(resEntities))
			return null;
		List<T> toReturn = new ArrayList<T>();
		for (HttpEntity entry : resEntities) {
			try {
				toReturn.add(serialize(entry, clazz));
			} catch (IOException e) {
				LOG.error(e.getLocalizedMessage(), e);
			}
		}
		return toReturn;
	}

	private <T> T serialize(HttpEntity resEntity, Class<T> clazz)
			throws IOException {
		String response = EntityUtils.toString(resEntity);
		T entity = new ObjectMapper().readValue(response, clazz);
		return entity;
	}
}
