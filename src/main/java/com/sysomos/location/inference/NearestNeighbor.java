package com.sysomos.location.inference;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.util.CollectionUtils;

import com.sysomos.location.exception.GeoLocationFinderException;
import com.sysomos.location.utils.Counter;
import com.sysomos.location.utils.MapUtils;
import com.sysomos.location.utils.MapUtils.SortingOrder;
import com.sysomos.location.utils.Pair;

public class NearestNeighbor {

	public static <U, V> Map<U, V> knn(final Map<Pair<U, U>, Double> distances,
			final Map<U, V> classes, int k) throws GeoLocationFinderException {

		if (distances == null || k <= 0 || classes == null)
			throw new GeoLocationFinderException("Illegal Arguments exception.");

		Map<Pair<U, U>, Double> sorted = MapUtils.sortByValue(distances,
				SortingOrder.DESC);

		Map<U, V> inferred = new HashMap<U, V>();

		for (Pair<U, U> pair1 : sorted.keySet()) {
			if (classes.containsKey(pair1.getA())
					|| !classes.containsKey(pair1.getB())) {
				continue;
			}

			U lhs = pair1.getA();
			int nn = 1;
			Counter<V> counter = new Counter<V>();
			Iterator<Pair<U, U>> iter = sorted.keySet().iterator();
			while (iter.hasNext() && nn <= k) {
				Pair<U, U> pair2 = iter.next();
				if (pair2.getA() == lhs && classes.containsKey(pair2.getB())) {
					counter.add(classes.get(pair2.getB()));
					nn++;
				}
			}
			List<V> value = counter.getTop(1);
			if (!CollectionUtils.isEmpty(value))
				inferred.put(lhs, value.get(0));
		}
		System.out.println("*** Inference Results Size: " + inferred.size());
		return inferred;
	}
}
