package com.sysomos.location.inference.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import com.google.common.collect.ImmutableList;

public class FetchTweetsTest {

	private static final List<Long> IDs = new ArrayList<Long>();
	private static final long StartDateMillis, EndDateMillis;

	static {
		IDs.add(223186092L);
		IDs.add(89848070L);
		IDs.add(2901854536L);
		IDs.add(2485244719L);
		IDs.add(1287002088L);

		StartDateMillis = new DateTime().minusDays(10).getMillis();
		EndDateMillis = new DateTime().minusDays(1).getMillis();
	}

	@Rule
	public TestRule watcher = new TestWatcher() {
		@Override
		protected void starting(Description description) {
			System.out.println("Starting test: " + description.getMethodName());
		}
	};

	@Before
	public void setUp() {

	}

	@Test
	public void fetchTest() {
		ImmutableList<Long> iList = new ImmutableList.Builder<Long>()
				.addAll(IDs).build();
		Map<Long, Integer> map = new HashMap<Long, Integer>();
		FetchTweets.fetch(iList, StartDateMillis, EndDateMillis, map);
	}
}
